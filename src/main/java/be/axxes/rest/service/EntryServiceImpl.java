package be.axxes.rest.service;

import be.axxes.rest.model.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class EntryServiceImpl implements EntryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntryServiceImpl.class);
    private static int idCounter = 1;

    private Map<String, Entry> entries = new HashMap<String, Entry>();

    @PostConstruct
    public void init() {
        putEntry("value of 1");
        putEntry("value of 2");
        putEntry("value of 3");
        putEntry("value of 4");
        putEntry("value of 5");
    }

    private synchronized Entry putEntry(final String value) {
        //TODO add input validation and detect duplicates.
        Entry entry = new Entry(Integer.toString(idCounter), value);
        entries.put(entry.getId(), entry);
        idCounter++;
        return entry;
    }

    @Override
    public Entry getEntry(final String id) {
        LOGGER.debug("retrieving value {}", id);
        return entries.get(id);
    }

    @Override
    public void deleteEntry(final String id) {
        entries.remove(id);
    }

    @Override
    public Entry create(final String value) {
        return putEntry(value);
    }
}
