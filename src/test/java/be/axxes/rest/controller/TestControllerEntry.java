package be.axxes.rest.controller;

import org.junit.Test;

import java.text.MessageFormat;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestControllerEntry extends BaseControllerIT {

    private static final String BODY_TEMPLATE = "'{'\"value\" : \"{0}\"'}'";

    @Test
    public void testCreate() throws Exception {
        String value = "value x";
        performPost("/entries", MessageFormat.format(BODY_TEMPLATE, value))
                .andExpect(status().isCreated())
        ;
        performGetJson("/entries/6")
                .andExpect(status().isOk())
                .andExpect(jsonPath(".value").value(value))
        ;
    }

    /*@Test
    public void testCreateWithLocationHeader() throws Exception {
        String value = "value x location";
        ResultActions resultActions = performPost("/entries", MessageFormat.format(BODY_TEMPLATE, value));
        resultActions
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/entries/6"))
        ;

        performGetJson(resultActions.andReturn().getResponse().getHeader("Location"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(".value").value(value))
        ;
    } */

    @Test
    public void testCreateDuplicate() throws Exception {
        String value = "value duplicate";
        performPost("/entries", MessageFormat.format(BODY_TEMPLATE, value))
                .andExpect(status().isCreated());
        performPost("/entries", MessageFormat.format(BODY_TEMPLATE, value))
                .andExpect(status().isConflict());

    }

    @Test
    public void testCreateInvalid() throws Exception {
        String value = "";
        performPost("/entries", MessageFormat.format(BODY_TEMPLATE, value))
                .andExpect(status().isUnprocessableEntity());
    }

}
